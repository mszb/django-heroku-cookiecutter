# MSZB-Cookiecutter-backend-app

A [cookiecutter](https://github.com/audreyr/cookiecutter) template for Django 2+ / Python 3 only, that is *really* 
optimized for running on twelve-factor-app platforms (like Heroku or DigitalOcean).

## Requirements

This cookiecutter template uses features that exists only in cookiecutter 0.9.0 or higher.


## Features

- Python 3 only, sorry guys but I had to move on...
- For [Django 2](https://docs.djangoproject.com/en/1.11/)+
- Heroku optimized stack
- Static served by `whitenoise` from the django app (advice to setup a cache instance above like CloudFlare)
- Instructions on how to configure the Amazon S3 bucket
- Instructions on how to deploy the app in less than 5 minutes


## Q&A
#### Why using `waitress` as the production server
> Gunicorn is actually designed to be used behing a buffering reverse proxy (e.g. nginx). In other words, without 
this buffering reverse proxy you expose your production server to a *slow network* attacks. If you want to dig on 
this particular topic, read the great article of @etianen 
*[Don't use Gunicorn to host your Django sites on Heroku](http://blog.etianen.com/blog/2014/01/19/gunicorn-heroku-django/)*


## Usage

First, get cookiecutter.

```sh
$ pip install cookiecutter
```

Now run it against this repo.

```sh
$ cookiecutter https://bitbucket.org/mszb/django-heroku-cookiecutter.git
```

You'll be prompted for some questions, answer them, then it will create a Django project for you.

NB! Be prepared for correct generation of deployment scripts and instructions you need to have Amazon account 
