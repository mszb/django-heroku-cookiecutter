# Wheel 0.25+ needed to install certain packages on CPython 3.5+
# like Pillow and psycopg2
# See http://bitly.com/wheel-building-fails-CPython-35
# Verified bug on Python 3.5.1
wheel==0.30.0

# Bleeding edge Django
django==2.1.1

# Password hasher
argon2-cffi==16.3.0

# Configuration
django-environ==0.4.4

# Database
psycopg2==2.7.3.1

# Assets
whitenoise==3.3.1

# Storage
django-storages==1.6.5

# Time zones support
pytz==2017.2

# Unicode slugification
awesome-slugify==1.6.5

# Django-rest-framework
djangorestframework==3.8.2
drf-openapi==1.0.0

# Your custom requirements go here

django-filter==2.0.0