# coding=utf-8
from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = '{{cookiecutter.project_slug}}.api'
    verbose_name = "API"

    def ready(self):
        """Override this to put in:
            Users system checks
            Users signal registration
        """
        pass
