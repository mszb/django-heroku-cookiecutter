# coding=utf-8
from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = '{{cookiecutter.project_slug}}.core'
    verbose_name = "Core"

    def ready(self):
        """Override this to put in:
            Users system checks
            Users signal registration
        """
        pass
