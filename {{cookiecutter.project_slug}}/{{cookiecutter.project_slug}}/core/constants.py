# coding=utf-8
""" Constants for the core apps. """


class Status(object):
    """Health statuses."""
    OK = u"OK"
    UNAVAILABLE = u"UNAVAILABLE"
