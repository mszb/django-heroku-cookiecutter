# coding=utf-8
"""Test core.views."""

from django.db import DatabaseError
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.test import TestCase
from mock import mock

from {{cookiecutter.project_slug}}.core.constants import Status


User = get_user_model()


class HealthTests(TestCase):
    """Tests of the health endpoint."""

    def test_all_services_available(self):
        """Test that the endpoint reports when all services are healthy."""
        self._assert_health(200, Status.OK, Status.OK)

    def test_database_outage(self):
        """Test that the endpoint reports when the database is unavailable."""
        with mock.patch('django.db.backends.base.base.BaseDatabaseWrapper.cursor', side_effect=DatabaseError):
            self._assert_health(503, Status.UNAVAILABLE, Status.UNAVAILABLE)

    def _assert_health(self, status_code, overall_status, database_status):
        """Verify that the response matches expectations."""
        response = self.client.get(reverse('health'))
        self.assertEqual(response.status_code, status_code)
        self.assertEqual(response['content-type'], 'application/json')

        expected_data = {
            'overall_status': overall_status,
            'detailed_status': {
                'database_status': database_status
            }
        }

        self.assertJSONEqual(response.content, expected_data)
