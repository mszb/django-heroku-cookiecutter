# -*- coding: utf-8 -*-
""" Core views. """

from django.db import transaction, connection, DatabaseError
from django.http import JsonResponse
from django.views.generic import TemplateView

from .constants import Status


@transaction.non_atomic_requests
def health(_):
    """Allows a load balancer to verify this service is up.

    Checks the status of the database connection on which this service relies.

    Returns:
        HttpResponse: 200 if the service is available, with JSON data indicating the health of each required service
        HttpResponse: 503 if the service is unavailable, with JSON data indicating the health of each required service

    Example:
        >>> response = requests.get('[domain_name]/health')
        >>> response.status_code
        200
        >>> response.content
        '{"overall_status": "OK", "detailed_status": {"database_status": "OK", "lms_status": "OK"}}'
    """

    try:
        cursor = connection.cursor()
        cursor.execute("SELECT 1")
        cursor.fetchone()
        cursor.close()
        database_status = Status.OK
    except DatabaseError:
        database_status = Status.UNAVAILABLE

    overall_status = Status.OK if (database_status == Status.OK) else Status.UNAVAILABLE

    data = {
        'overall_status': overall_status,
        'detailed_status': {
            'database_status': database_status,
        },
    }

    if overall_status == Status.OK:
        return JsonResponse(data)
    else:
        return JsonResponse(data, status=503)


class RobotsView(TemplateView):
    """To control amount of requests from search engines.
    see http://www.robotstxt.org/robotstxt.html
    """
    template_name = "robots.txt"
    content_type = 'text/plain'


class HomePageView(TemplateView):
    """Dummy home page.
    """
    template_name = "core/index.html"
