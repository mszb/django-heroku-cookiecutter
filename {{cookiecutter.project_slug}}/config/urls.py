# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework.documentation import include_docs_urls

from {{cookiecutter.project_slug}}.core import views as core_views

urlpatterns = [
    path('robots\.txt', core_views.RobotsView.as_view(), name="robots"),
    path('health/', core_views.health, name='health'),

    re_path(r'^api/', include('{{cookiecutter.project_slug}}.api.urls'), name='api'),
    re_path(r'^api-auth/', include('rest_framework.urls'), name='rest_framework'),
    re_path(r'^api-docs/', include_docs_urls(title="{{cookiecutter.project_name}} API title", public=False)),

    path('', core_views.HomePageView.as_view(), name='home'),
    path(settings.ADMIN_URL, admin.site.urls),
]

if settings.DEBUG:
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [
                          path('__debug__/', include(debug_toolbar.urls)),
                      ] + urlpatterns
